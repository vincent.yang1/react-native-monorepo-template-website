import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: '快速使用',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        模板程序已实现了公共模块common和业务模块module-a和module-b
      </>
    ),
  },
  {
    title: '保持更新',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        目前基于最新版RN0.72.0版本搭建，默认开启新架构，保证最新的技术栈
      </>
    ),
  },
  {
    title: '快速迭代',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        微前端的组织架构方式，方便不同业务需求快速开发迭代，并必要时可以导出对外提供服务
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
