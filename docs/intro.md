---
sidebar_position: 1
---

# 快速引导



## 创建app


执行以下命名，将成长我们想要的app:

```bash
npx react-native init SomeApp --template https://gitlab.com/vincent.yang1/react-native-monorepo-template.git

```


## 开发app

执行以下命令:

```bash
cd SomeApp
react-native run-android
```


